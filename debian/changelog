libowl-directsemantics-perl (0.001-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * use salsa.debian.org in Vcs-* headers

  [ gregor herrmann ]
  * use MetaCPAN URIs (not search.cpan.org or www.cpan.org)

  [ Debian Janitor ]
  * use secure copyright file specification URI
  * set upstream metadata fields:
    Bug-Database Repository

  [ Jonas Smedegaard ]
  * simplify rules;
    stop build-depend on dh-buildinfo devscripts cdbs
  * stop build-depend explicitly on perl:
    not called directly during build
  * use debhelper compatibility level 13 (not 7);
    build-depend on debhelper-compat (not debhelper)
  * relax to (build-)depend unversioned on libmoose-perl:
    required version satisfied in all supported Debian releases
  * relax to drop ancient fallback (build-)dependencies
  * annotate test-only build-dependencies
  * enable autopkgtest
  * set Rules-Requires-Root: no
  * declare compliance with Debian Policy 4.6.0
  * update watch file:
    + use file format 4
    + drop unused option uversionmangle
    + mention gbp --uscan in usage comment
    + use substitution strings
  * update git-buildpackage config: avoid any .git* files
  * update copyright info:
    + use License-Grant and Reference fields;
      add lintian overrides about Reference field
    + update coverage
    + use secure URI for Contact-Info
    + use MetaCPAN (not cpan) as Source URI
  * set environment variable NO_AUTO_INSTALL
    to avoid use of Module::AutoInstall during build,
    and add patch 1001 to use shared module Module::Package;
    build-depend on recent libmodule-package-rdf-perl
  * move aside stale copy of Scalar::Util during build;
    closes: bug#975210, thanks to Lucas Nussbaum and Gregor Herrmann

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 11 Sep 2021 22:19:52 +0200

libowl-directsemantics-perl (0.001-2) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Use canonical hostname (anonscm.debian.org) in Vcs-Git URI.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Install examples.
  * Drop patch 1001: Build problem turned out to be (and fixed by now)
    in CDBS.
  * Bump standards-version to 3.9.4.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include current year.
  * Update package relations:
    + Relax to build-depend unversioned on cdbs: Needed version
      satisfied even in oldstable.
  * Fix use comment and license pseudo-sections in copyright file to
    obey silly restrictions of copyright format 1.0.

  [ gregor herrmann ]
  * Explicitly (build) depend on Module::Pluggable. (Closes: #755024)

  [ Damyan Ivanov ]
  * use canonical Vcs-Git URL

 -- Damyan Ivanov <dmn@debian.org>  Mon, 28 Jul 2014 15:28:04 +0300

libowl-directsemantics-perl (0.001-1) unstable; urgency=low

  * New upstream release.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 28 Jun 2012 16:52:13 +0200

libowl-directsemantics-perl (0.000~03-1) experimental; urgency=low

  * Initial packaging release.
    Closes: bug#679385.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 28 Jun 2012 12:17:08 +0200
